from .chan import Chan
from helpers import unescape_entities

# Who is this four-chan?
class FourChan(Chan):
    url_boards = 'https://a.4cdn.org/boards.json'
    url_catalog = 'https://a.4cdn.org/%s/catalog.json'
    url_file = 'https://i.4cdn.org/%s/%i.%s'
    url_thread = 'https://a.4cdn.org/%s/thread/%i.json'

    def get_boards(self, boards):
        return boards['boards']

    def get_board_name(self, board):
        return board['board']

    def get_board_title(self, board):
        return board['title']

    def get_page_threads(self, page):
        return page['threads']

    def get_pages(self, pages):
        return pages

    def get_thread_description(self, thread):
        return thread['com']

    def get_thread_number(self, thread):
        return thread['no']

    def get_thread_posts(self, thread):
        return thread['posts']

    def get_thread_reply_count(self, thread):
        return thread['replies']

    def get_thread_sticky(self, thread):
        try:
            return thread['sticky'] == 1
        except KeyError:
            return False

    def get_thread_subject(self, thread):
        try:
            subject = thread['sub']

            subject = unescape_entities(subject)

            return subject
        except KeyError:
            return ''

    def get_threads(self, page):
        return page['threads']

    def post_has_file(self, post):
        return 'filename' in post

    def post_filename(self, post):
        return post['tim']

    def post_filepath(self, post):
        return '%s.%s' % (post['tim'], post['ext'][1:])

    def post_filetype(self, post):
        return post['ext'][1:]

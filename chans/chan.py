from helpers import get_url_json

class Chan:
    boards = {}
    cache = {}

    url_boards = ''
    url_catalog = ''
    url_file = ''
    url_thread = ''

    def board_cache(self, board):
        return self.cache[board]

    def board_cache_set(self, board, cache_new):
        self.cache[board] = cache_new

    def board_in_cache(self, board):
        return board in self.cache

    def thread_cache(self, board, thread_number):
        try:
            return self.cache[board][thread_number]
        except KeyError:
            return {}

    def thread_cache_set(self, board, thread):
        thread_id = self.get_thread_number(thread)

        self.cache[board][thread_id] = thread

    def thread_in_cache(self, board, thread_number):
        return thread_number in self.board_cache(board)

    def thread_cache_modified(self, board, thread):
        no = self.get_thread_number(thread)
        thread_prior = self.thread_cache(board, no)

        return thread_prior != thread

    def cache_board_list(self):
        self.boards = {}

        for board in get_url_json(self.url_boards)['boards']:
            self.boards[self.get_board_name(board)] = {
                'name': self.get_board_name(board),
                'title': self.get_board_title(board)
            }

    def check_board_exists(self, board):
        if self.boards == {}:
            self.cache_board_list()

        return board in self.boards

from chans.fourchan import FourChan
from helpers import date, get_url_json, folder_output, save_file_from_url, \
    string_has_keyword
import json
import os
import sys
import time
from urllib.error import HTTPError
import yaml

with open('config.yml') as f:
    config = yaml.load(f)

# Create the cache folder if it does not exist.
if not os.path.exists(config['cache']):
    os.makedirs(config['cache'])

# Configure the chans.
chans = {}
chans['4chan'] = FourChan()

# Create cache folders for each chan and files for their boards.
for key, chan in chans.items():
    chan_cache_dir = config['cache'] + '/' + key

    if not os.path.exists(chan_cache_dir):
        os.makedirs(chan_cache_dir)

    chan.cache_board_list()

    for key, board in chan.boards.items():
        board_cache_path = chan_cache_dir + '/' + board['name'] + '.json'

        if not os.path.isfile(board_cache_path):
            board_default_data = {}

            with open(board_cache_path, 'w') as f:
                json.dump(board_default_data, f)

    chan.boards = {}

# Ensure that each runner is valid.
runner_count = 0

for c in config['runners']:
    if c['type'] == 'search':
        msg_base = 'Runner {}, {}/{} with keywords {} '.\
            format(runner_count, c['chan'], c['board'], c['keywords'])
    elif c['type'] == 'board':
        msg_base = 'Runner {}, {}/{}'.\
            format(runner_count, c['chan'], c['board'])

    if c['chan'] not in chans:
        print(msg_base + 'has invalid chan!')

        sys.exit(1)

    if not chans[c['chan']].check_board_exists(c['board']):
        print(msg_base + 'has invalid board!')

        sys.exit(1)

    runner_count += 1

while True:
    catalog_cache = {}

    for runner in config['runners']:
        b = runner['board']
        c = runner['chan']
        rtype = runner['type']

        if rtype == 'search':
            keys = runner['keywords']
            print('[%s] Running %s:%s/%s/%s' % (date(), rtype, c, b, keys))
        elif rtype == 'board':
            print('[%s] Running %s:%s/%s' % (date(), rtype, c, b))

        board_thread_cache = '%s/%s/%s.json' % (config['cache'], c, b)

        with open(board_thread_cache) as f:
            thread_cache = json.load(f)

        thread_new_cache = {}

        catalog_url = chans[c].url_catalog % (b)

        # Check if the board is not already in the cache.
        if not chans[c].board_in_cache(b):
            chans[c].board_cache_set(b, {})

        resp = get_url_json(catalog_url)

        # Cycle through the threads for those with the keyword/s.
        for page in chans[c].get_pages(resp):
            for thread in chans[c].get_threads(page):
                # If the thread is sticky, ignore it. Safe to presume these are
                # rule threads.
                if chans[c].get_thread_sticky(thread):
                    continue

                thread_id = chans[c].get_thread_number(thread)

                try:
                    thread_last_replies = thread['last_replies']
                except KeyError:
                    thread_last_replies = {}

                # Add the thread to the new cache.
                thread_new_cache[thread_id] = {
                    'id': thread_id,
                    'replies': thread['replies'],
                    'last_replies': thread_last_replies
                }

                # Check if one or more of the keywords is in the thread subject.
                subject = None

                try:
                    subject = chans[c].get_thread_subject(thread)

                    within = string_has_keyword(subject, keys)
                except (KeyError, NameError):
                    within = False

                if not within:
                    # If the subject is nothing, try the description.
                    subject_len = len(chans[c].get_thread_subject(thread))

                    if subject_len == 0:
                        try:
                            desc = chans[c].get_thread_description(thread)

                            within = string_has_keyword(desc, keys)
                        except (KeyError, NameError):
                            within = False

                # If there are no keywords in the thread subject, skip it.
                if rtype == 'search' and not within:
                    continue

                # If there has been no change in the thread, skip it.
                if str(thread_id) in thread_cache:
                    t_replies = chans[c].get_thread_reply_count(thread)
                    t_cache_replies = thread_cache[str(thread_id)]['replies']

                    if t_replies == t_cache_replies:
                        continue

                thread_url = chans[c].url_thread % (b, thread_id)
                resp = get_url_json(thread_url)

                posts = chans[c].get_thread_posts(resp)

                thread['posts'] = posts

                chans[c].thread_cache_set(b, thread)

                thread_images_saved = 0

                for post in posts:
                    # If the post does not have a file, skip it.
                    if not chans[c].post_has_file(post):
                        continue

                    # Check if the post's file is in the filetype list. If it is
                    # not, ignore the file.
                    filetypes = config['accepted-filetypes']

                    if not chans[c].post_filetype(post) in filetypes:
                        continue

                    # If the file already exists, skip it.
                    filename = chans[c].post_filename(post)
                    filetype = chans[c].post_filetype(post)
                    folder = folder_output(runner, chans[c], thread)

                    # Create the folder if it doesn't exist.
                    if not os.path.exists(folder):
                        os.makedirs(folder)

                    filepath = '%s/%s.%s' % (folder, filename, filetype)

                    if os.path.exists(filepath):
                        continue

                    file_url = chans[c].url_file % (b, filename, filetype)

                    try:
                        save_file_from_url(file_url, filepath)

                        thread_images_saved += 1
                    except HTTPError:
                        pass

                msg_saved = '[%s] %i images saved from %s'
                tc = date()

                tis = thread_images_saved
                if subject:
                    print(msg_saved % (tc, tis, subject))
                else:
                    name = str(thread_id)
                    print(msg_saved % (tc, tis, name))

        with open(board_thread_cache, 'w') as f:
            print("Saving new thread cache for " + b)
            json.dump(thread_new_cache, f)

    interval = int(config['interval'])

    # Chans usually set a minimum of 10 seconds or so for updating. So, if the
    # interval is lower than that, then set the interval to 10 to be safe.
    if interval < 10:
        interval = 10

    print('[%s] Sleeping for %i...' % (date(), interval))

    time.sleep(interval)

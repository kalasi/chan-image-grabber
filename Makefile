#!/bin/bash

start: install run

install:
	python3 -m venv .
	source ./bin/activate; \
	pip3 install -r requirements.txt; \

run:
	@(source ./bin/activate && python3 .) || echo "run 'make install' first"

update:
	git pull

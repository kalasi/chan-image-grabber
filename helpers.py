import json
import sys
import time

# Python 2 and 3 use different libraries for urllib, so try importing
# Python 3's first, and if that fails, import Python 2's.
try:
    from urllib.request import Request, urlopen, urlretrieve
except ImportError:
    from urllib import urlretrieve
    from urllib2 import Request, urlopen

# Python 3 has the html library for unescaping HTML entities while Python 2
# has the HTMLParser library. Try importing Python 3 first.
try:
    import html
except ImportError:
    import HTMLParser

def date():
    return time.strftime('%H:%M:%S')

def folder_output(runner, chan, thread):
    folder = runner['output-folder']

    # Subject: Remove forward slashes.
    subject = chan.get_thread_subject(thread).replace('/', '')

    if len(subject) == 0:
        subject = str(chan.get_thread_number(thread))

    folder = folder.replace('{subject}', subject)

    return folder

def get_url_json(url):
    request = Request(url)

    response = urlopen(request)

    data = response.read()
    data = data.decode('UTF-8')

    return json.loads(data)

def string_has_keyword(string, keywords, case_sensitive=False):
    if not case_sensitive:
        string = string.lower()

    return any(s in string for s in keywords)

def save_file_from_url(url, filepath):
    urlretrieve(url, filepath)

def unescape_entities(string):
    if sys.version_info > (3, 0, 0):
        subject = html.unescape(string)
    else:
        parser = HTMLParser.HTMLParser()

        subject = parser.unescape(string)

    return subject

# chan-image-grabber

A continuously running Python script to grab images from preconfigured threads
or boards on Chan imageboards. Searching by thread subject keywords (or
description as a fallback if there is no subject) is supported as well as
grabbing from all threads in a single board.

_disclaimer: neither directly nor indirectly associated with any Chan,
imageboard, or website that is supported by this software._

### Supported Python versions

Python 3.4+.

### Installation

1. `git clone https://github.com/taiyaeix/chan-image-grabber`
2. `cd chan-image-grabber`
3. `make install`

If you do not have `make`, then use the following instead:

1. `pip3 install -r requirements.txt`
2. `python3 .`

### Usage

`make run` after install or `python3 .` if you do not have `make`.

To update the installation, use `make update`.

Use `make start` to install dependencies _and_ run the script.

### Configuration options

- `accepted_filetypes`: A list of filetypes that will be downloaded. If the
  accepted filetypes list has `jpg` and `png` but the filetype for an image
  is `gif`, then the file will be ignored.
- `interval`: The number of seconds (must be above 10) to wait before
  starting another cycle of image downloading.
- `runners`
    - `chan`: The Chan imageboard to search from.
    - `board`: The acronym/name of the board, such as `e` or `wg`.
    - `keywords`: A list of keywords to search for in thread subjects. If a
       thread does not have a subject, then the description is searched as
       a fallback.
    - `output-folder` The folder to output to. This can be either relative
      to the installation folder (`./pics`) or absolute (`/mnt/hdd1/pics`).
    - `type`: The type of configuration to use, options are below.

### Supported configurations

Configuring to search for keywords in threads is supported:

```yaml
accepted-filetypes:
  - gif
  - jpeg
  - jpg
  - png
  - webm
interval: 600
runners:
  - chan: 4chan
    board: w
    keywords:
      - minimal
      - plain
    output-folder: ./pics/plain
    type: search
```

In addition, retrieving all in a board is supported:

```yaml
accepted-filetypes:
  - gif
  - jpeg
  - jpg
  - png
  - webm
interval: 600
runners:
  - chan: 4chan
    board: an
    output-folder: ./pics/animals
    type: board
```

Notice the different `type` in the runner.

### Dynamic output folders

Output folder can be dynamic. Right now there's just 1 dynamic option, but
there will be more in the future. This allows for the output folders for
runners to be more distinguished. Here are the dynamic output options:

| Name    | output-folder example    | Description                       |
| ------- | ------------------------ | --------------------------------- |
| Subject | ./pics/animals/{subject} | Folder equal to thread subject/ID |

For the subject, if the thread has no subject, then the ID of the thread
(number) is instead used.

### What Chans are supported?

4chan. Infinitechan support is arriving.

### Why don't the pictures save?

Make sure the user the script is running under has write permissions to the
folder.

### Why a 10 second minimum interval?

4chan's API enforces a 10 second thread updating minimum, and 10 seconds
sounds like a good number.

### Can you add support for this other Chan?

Make an issue with a reasonable description and the answer will probably be yes.

### What's the resource usage like?

0% (< 1%) CPU at all times with a max of 20MiB RAM usage (garbage
collection kicks in every now and then dropping it back down to 10MiB).
Your real concern is having large enough hard drive/s and enough
bandwidth.
